package view;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JTextArea;

public class PanelResultado extends Panel
{
	
	private static final long serialVersionUID = 1L;
	private JTextArea paisResultante;
	
	PanelResultado(String resultado)
	{
		this.setLayout(null);
		this.crearComponentes(resultado);
		this.setBackground(new Color(86, 7, 12).brighter());
		this.setVisible(true);
	}
	

//***********************************************************METODOS AUXILIARES	
	
	private void crearComponentes(String resultado) 
	{
		this.paisResultante = new JTextArea("Pienso en: \n"+resultado);
		this.crearAreaDeTexto(paisResultante, 90, 80, 200, 60);
	}
	
	
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		Image akinatorJuega = null;
		Image globoDeDialogo = null;
		
		akinatorJuega = this.cargarImagen("src/view/akinatorJuega.png");
		globoDeDialogo = this.cargarImagen("src/view/dialogo.png");
		
		g.drawImage(akinatorJuega, 190, 20, 300, 400, null);
		g.drawImage(globoDeDialogo, -60, -50, 400, 300, null);
	}
}
