package view;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JTextArea;



class PanelJuego extends Panel
{

	private static final long serialVersionUID = 1L;
	
	private JTextArea pregunta;
	
	
	PanelJuego()//CONSTRUCTOR
	{
		this.setLayout(null);
		this.crearComponentes();
		this.setBackground(new Color(86, 7, 12).brighter());
		this.setVisible(true);
	}
	
	
	public void setPregunta(String preg)
	{
		if(preg.length() > 28)
		{
			preg = ordenarPregunta(preg);
		}
		this.pregunta.setText(preg);
	}
	
	
//*********************************************** METODOS AUXILIARES
	
	
	private void crearComponentes() 
	{
		this.pregunta = new JTextArea("");
		this.crearBarra(5, 430, 480, 2);
		this.crearAreaDeTexto(this.pregunta, 20, 90, 300, 40);
	}
	
	
	
	//DIBUJA LAS IMAGENES
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		Image akinatorJuega = null;
		Image globoDeDialogo = null;
		
		akinatorJuega = this.cargarImagen("src/view/akinatorJuega.png");
		globoDeDialogo = this.cargarImagen("src/view/dialogo.png");
		
		g.drawImage(akinatorJuega, 190, 20, 300, 400, null);
		g.drawImage(globoDeDialogo, -60, -50, 400, 300, null);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
