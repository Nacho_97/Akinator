package view;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JTextArea;

public class PanelGana extends Panel
{
	private static final long serialVersionUID = 1L;
	private JTextArea dialogo;
	PanelGana()
	{
		this.setLayout(null);
		this.crearComponentes();
		this.setBackground(new Color(86, 7, 12).brighter());
		this.setVisible(true);
	}

	private void crearComponentes() 
	{
		this.dialogo = new JTextArea("�Lo logr�!\njuguemos devuelta");
		this.crearAreaDeTexto(dialogo, 60, 90, 200, 60);
	}
	
	
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		Image akinator = null;
		Image globoDeDialogo = null;
		
		akinator = this.cargarImagen("src/view/akinatorGana.png");
		globoDeDialogo = this.cargarImagen("src/view/dialogo.png");
		
		
		g.drawImage(akinator, 190, 20, 300, 400, null);
		g.drawImage(globoDeDialogo, -60, -50, 400, 300, null);
	}
}
