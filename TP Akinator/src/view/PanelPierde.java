package view;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class PanelPierde extends Panel
{
	private static final long serialVersionUID = 1L;
	private String paisResultante;
	private JLabel jlNuevoPais;
	private JLabel jlnuevaPregunta;
	
	private JTextField jtPais;
	private JTextField jtPregunta;
	private JTextArea dialogo;
	
	
	PanelPierde(String paisResult)
	{
		this.paisResultante = paisResult;
		this.setLayout(null);
		this.crearComponentes();
		this.setBackground(new Color(86, 7, 12).brighter());
		this.setVisible(true);
	}

	
	public String getPregunta()
	{
		return this.jtPregunta.getText();
	}
	
	
	public String getPais()
	{
		return this.jtPais.getText();
	}
	
	
//***********************************************************************************METODOS AUXILIARES
	
	private void crearComponentes() 
	{
		this.jlNuevoPais = new JLabel("�Cu�l es su pais?");
		this.jlnuevaPregunta = new JLabel("�Que diferencia a "+this.paisResultante+" de su pais?");
		this.jlNuevoPais.setBounds(20, 280, 100, 25);
		this.jlnuevaPregunta.setBounds(20, 340, 300, 25);
		this.add(jlNuevoPais);
		this.add(jlnuevaPregunta);
		
		this.jtPais = new JTextField();
		this.jtPregunta = new JTextField();
		this.jtPais.setBorder(null);
		this.jtPregunta.setBorder(null);
		this.jtPais.setBounds(20, 305, 100, 25);
		this.jtPregunta.setBounds(20, 365, 300, 25);
		this.add(this.jtPais);
		this.add(this.jtPregunta);
		
		this.dialogo = new JTextArea("Casi lo logr�\npero la proxima no fallo");
		this.crearAreaDeTexto(dialogo, 60, 90, 200, 60);
	}
	
	
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		Image akinatorInvita = null;
		Image globoDeDialogo = null;
		
		akinatorInvita = this.cargarImagen("src/view/akinatorPierde.png");
		globoDeDialogo = this.cargarImagen("src/view/dialogo.png");
			
		g.drawImage(akinatorInvita, 190, 20, 300, 400, null);
		g.drawImage(globoDeDialogo, -70, -50, 400, 300, null);
	}
}
