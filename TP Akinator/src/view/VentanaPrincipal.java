package view;


import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

import presenter.Controlador;



public class VentanaPrincipal extends JFrame implements ActionListener
{
	private static final long serialVersionUID = 1L;
	private PanelDePresentacion panelDePresentacion;
	private PanelJuego panelJuego;
	private PanelJuegoTerminado panelJuegoTerminado;
	private PanelResultado panelResultado;
	private PanelPierde panelPierde;
	private PanelGana panelGana;
	
	private Controlador controlador;
	
	public VentanaPrincipal(String titulo, Controlador ctrl)
	{
		this.controlador = ctrl;
		this.controlador.setVentana(this);
		this.crearPanelPresentacion();
		this.configurarVentana(titulo);
	}
	
	
	public void mostrarPregunta(String pregunta)
	{
		this.panelJuego.setPregunta(pregunta);
	}
	
	
	public String getNuevaPregunta()
	{
		return this.panelPierde.getPregunta();
	}
	
	
	public String getNuevoPais()
	{
		return this.panelPierde.getPais();
	}
	
	
	public void crearPanelPresentacion() 
	{
		this.getContentPane().removeAll();
		this.panelDePresentacion = new PanelDePresentacion();
		this.panelDePresentacion.add(this.crearBoton("Jugar", 20, 395, 300, 50));
		this.getContentPane().add(panelDePresentacion);
		this.setVisible(true);
		this.repaint();
	}
	
	
	public void crearPanelJuego() 
	{
		this.getContentPane().removeAll();
		this.panelJuego = new PanelJuego();
		this.panelJuego.add(this.crearBoton("Si", 60, 280, 50, 25));
		this.panelJuego.add(this.crearBoton("No", 130, 280, 50, 25));
		this.panelJuego.add(this.crearBoton("Juego nuevo", 10, 440, 125, 25));
		this.getContentPane().add(panelJuego);
		this.setVisible(true);
		this.repaint();
	}
	
	
	public void crearPanelResultado(String resultado)
	{
		this.getContentPane().removeAll();
		this.panelResultado = new PanelResultado(resultado);
		this.panelResultado.add(this.crearBoton("s�, es el pais", 60, 280, 150, 25));
		this.panelResultado.add(this.crearBoton("no, no es el pais", 60, 330, 150, 25));
		this.getContentPane().add(panelResultado);
		this.setVisible(true);
		this.repaint();
	}
	
	
	public void crearPanelJuegoTerminado()
	{
		this.getContentPane().removeAll();
		this.panelJuegoTerminado = new PanelJuegoTerminado();
		this.panelJuegoTerminado.add(this.crearBoton("Juego nuevo", 30, 270, 200, 40));
		this.panelJuegoTerminado.add(this.crearBoton("Salir", 30, 340, 200, 40));
		this.getContentPane().add(panelJuegoTerminado);
		this.setVisible(true);
		this.repaint();
	}
	
	
	public void crearPanelPierde(String resultado)
	{
		this.getContentPane().removeAll();
		this.panelPierde = new PanelPierde(resultado);
		this.panelPierde.add(this.crearBoton("Recordar", 150, 410, 200, 40));
		this.getContentPane().add(panelPierde);
		this.setVisible(true);
		this.repaint();
	}
	
	
	public void crearPanelGana()
	{
		this.getContentPane().removeAll();
		this.panelGana = new PanelGana();
		this.panelGana.add(this.crearBoton("Bien Hecho", 50, 320, 200, 40));
		this.getContentPane().add(this.panelGana);
		this.setVisible(true);
		this.repaint();
	}
	
	
//***********************************************METODOS AUXILIARES
	
	
	private JButton crearBoton(String titulo, int x, int y, int ancho, int alto)
	{
		JButton btn = new JButton(titulo);
		btn.setBounds(x, y, ancho, alto);
		btn.addActionListener(this);
		return btn;
	}
	
	
	//CONFIGURA LA VENTANA
	private void configurarVentana(String titulo) 
	{
		Toolkit miPantalla = Toolkit.getDefaultToolkit();
		Dimension resolucion = miPantalla.getScreenSize();
		
		int anchoPantalla = (int) resolucion.getWidth();
		int altoPantalla = (int) resolucion.getHeight();
		
		this.setSize(500, 500);
		this.setLocation(anchoPantalla/4, altoPantalla/4);
		

		this.setTitle(titulo);
		this.setResizable(false);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}


	public void actionPerformed(ActionEvent e) 
	{
		this.controlador.actionPerformed(e);
	}


	public static void main(String[] args) 
	{
		VentanaPrincipal miVentana = new VentanaPrincipal("Akinator", new Controlador());
		miVentana.setVisible(true);
	}



















}