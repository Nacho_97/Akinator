package view;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionListener;

import javax.swing.JTextArea;

public class PanelJuegoTerminado extends Panel implements ActionListener
{
	
	private static final long serialVersionUID = 1L;
	private JTextArea dialogo;
	
	PanelJuegoTerminado()
	{
		this.setLayout(null);
		this.crearComponentes();
		this.setBackground(new Color(86, 7, 12).brighter());
		this.setVisible(true);
	}
	
	
	public void crearComponentes()
	{
		this.dialogo = new JTextArea("¿Quieres jugar devuelta?");
		this.crearAreaDeTexto(dialogo, 50, 95, 200, 60);
	}
	
	
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		Image akinator = null;
		Image globoDeDialogo = null;
		
		akinator = this.cargarImagen("src/view/akinatorJuega.png");
		globoDeDialogo = this.cargarImagen("src/view/dialogo.png");
		
		
		g.drawImage(akinator, 190, 20, 300, 400, null);
		g.drawImage(globoDeDialogo, -60, -50, 400, 300, null);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
