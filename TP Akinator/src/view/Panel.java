package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class Panel extends JPanel implements ActionListener 
{
	
	private static final long serialVersionUID = 1L;
	
	
	
//*******************************************************METODOS ESTATICOS
	
	
	
	public static String ordenarPregunta(String preg) 
	{
		boolean noSeAplicoCambio = true;
		for(int i = 0 ; i < preg.length() ; i++)
		{
			if(preg.charAt(i) == ' ' && i>20 && i<=28 && noSeAplicoCambio|| i==28 && noSeAplicoCambio)
			{
				preg = aplicarNextLine(preg, i);
				noSeAplicoCambio = false;
			}
		}
		return preg;
	}
	
	
	
	public static String aplicarNextLine(String p, int posicion)
	{
		StringBuilder aux = new StringBuilder();
		StringBuilder pregunta = new StringBuilder(p);
		
		for(int i = pregunta.length()-1 ; i >= posicion ; i--)
		{
			aux.append(pregunta.charAt(i));
			pregunta.deleteCharAt(i);
			if(i == posicion)
			{
				pregunta.append("\n");
			}
		}
		pregunta = completarPregunta(pregunta, aux);
		return pregunta.toString();
	}
	
	
	
	public static StringBuilder completarPregunta(StringBuilder preguntaIncompleta, StringBuilder resto)
	{
		for(int i = resto.length()-1 ; i >=0 ; i--)
		{
			preguntaIncompleta.append(resto.charAt(i));
		}
		return preguntaIncompleta;
	}
	
	
//**********************************************************************METODOS DE CLASE
	
	
	protected void crearBarra(int x,int y, int ancho, int alto)
	{
		JSeparator separador = new JSeparator();
		separador.setBounds(x, y, ancho, alto);
		this.add(separador);
	}
	
	
	
	protected void crearCampoDeTexto(String txt, int x,int y, int ancho, int alto)
	{
		JTextField texto = new JTextField(txt);
		crearTexto(texto, x, y, ancho, alto);
	}
	
	
	
	protected void crearTexto(JTextField jTxt, int x,int y, int ancho, int alto)
	{
		jTxt.setBounds(x, y, ancho, alto);
		jTxt.setBorder(null);
		jTxt.setEditable(false);
		jTxt.setOpaque(false);
		jTxt.setHorizontalAlignment(SwingConstants.CENTER);
		jTxt.setBackground(new Color (255,191,0));
		jTxt.setFont(new Font(jTxt.getText(), 2,15));
		this.add(jTxt);
	}
	
	
	
	protected void crearAreaDeTexto(JTextArea jTxtA, int x,int y, int ancho, int alto)
	{
		jTxtA.setBounds(x ,y, ancho, alto);
		jTxtA.setBorder(null);
		jTxtA.setEditable(false);
		jTxtA.setOpaque(false);
		this.add(jTxtA);
	}
	
	
	
	//CARGA UNA IMAGEN QUE SE VAN A UTILIZAR Y LA RETORNA
	protected Image cargarImagen(String ruta)
	{
		File archivo = new File(ruta);
		
		try 
		{
			return ImageIO.read(archivo);	
		} 
		catch (IOException e) 
		{
			System.out.println("hubo un error al cargar la imagen");
			e.printStackTrace();
		}
		return null;
	}
	
	
	
	@Override
	public void actionPerformed(ActionEvent arg0) 
	{
		
	}

	
	
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
	}
	
	
	
}
