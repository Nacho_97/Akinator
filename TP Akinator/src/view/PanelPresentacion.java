package view;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import javax.swing.JTextArea;

class PanelDePresentacion extends Panel
{
	private static final long serialVersionUID = 1L;
	private JTextArea oracionInicial;
		
	PanelDePresentacion()
	{
		this.setLayout(null);
		this.crearComponentes();
		this.setBackground(new Color(86, 7, 12).brighter());
		this.setVisible(true);
	}
		
		
//*********************************************** METODOS AUXILIARES	
		
		
	private void crearComponentes()
	{
		this.oracionInicial = new JTextArea("�Hola soy Akinator!\n�En que pa�s pensas?\n�Ha que adivino!");
		this.crearAreaDeTexto(this.oracionInicial, 50, 80, 200, 60);
	}
		
		
		
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
			
		Image akinatorInvita = null;
		Image globoDeDialogo = null;
		//Image fondo = null;
		
		//fondo = this.cargarImagen("src/interfazGrafica/fondo3.jpg");
		akinatorInvita = this.cargarImagen("src/view/akinatorInvita.png");
		globoDeDialogo = this.cargarImagen("src/view/dialogo.png");
			
		//g.drawImage(fondo, 0, 0, 500, 500, null);
		g.drawImage(akinatorInvita, 190, 20, 300, 400, null);
		g.drawImage(globoDeDialogo, -70, -50, 400, 300, null);
	}
	
	
}
