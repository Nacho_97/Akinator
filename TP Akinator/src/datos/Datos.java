package datos;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import model.ArbolBinario;

public class Datos 
{
	ArbolBinario arbolDePreguntasYRespuestas;
	
	public Datos()
	{
		this.cargarDatos();
	}
	
	
	public ArbolBinario getArbol()
	{
		return this.arbolDePreguntasYRespuestas;
	}
	
	
	public void cargarDatos()
	{
		try 
		{
			ObjectInputStream recuperando_datos = new ObjectInputStream(new FileInputStream("src/datos/arbol.dat"));
			
			this.arbolDePreguntasYRespuestas = (ArbolBinario) recuperando_datos.readObject();
			
			recuperando_datos.close();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	
	public void guardarDatos(ArbolBinario ab)
	{
		try
		{
			ObjectOutputStream escribiendo_fichero = new ObjectOutputStream(new FileOutputStream("src/datos/arbol.dat"));
			
			escribiendo_fichero.writeObject(ab);
			
			escribiendo_fichero.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	
	public static void main(String[] args) 
	{
		ArbolBinario datos = new ArbolBinario();
		datos.setRaiz("�Su pais posee salida al mar?");
		datos.setDosElementosAPadre("ARGENTINA", "BOLIVIA", "�Su pais posee salida al mar?");
		
		try
		{
			ObjectOutputStream escribiendo_fichero = new ObjectOutputStream(new FileOutputStream("src/datos/arbol.dat"));
			
			escribiendo_fichero.writeObject(datos);
			
			escribiendo_fichero.close();
		}
		catch(Exception e)
		{
			
		}
	}
	
	
	
}
