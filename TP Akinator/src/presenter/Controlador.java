package presenter;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import model.Juego;
import view.VentanaPrincipal;

public class Controlador implements ActionListener
{
	private VentanaPrincipal view;
	private Juego juego;
	
	public Controlador()
	{
		this.juego = new Juego();
	}
	
	
	public void setVentana(VentanaPrincipal frame)
	{
		this.view = frame;
	}


	@Override
	public void actionPerformed(ActionEvent e) 
	{
		if(e.getActionCommand().equalsIgnoreCase("jugar"))
		{
			this.view.crearPanelJuego();
			actualizarPreguntaORespuesta();
		}
		else if(e.getActionCommand().equalsIgnoreCase("si"))
		{
			this.juego.respuestaAfirmativa();
			actualizarPreguntaORespuesta();
		}
		else if(e.getActionCommand().equalsIgnoreCase("no"))
		{
			this.juego.respuestaNegativa();
			actualizarPreguntaORespuesta();
		}
		else if(e.getActionCommand().equalsIgnoreCase("juego nuevo"))
		{
			this.juego.reiniciarJuego();
			this.view.crearPanelJuego();
			this.actualizarPreguntaORespuesta();
		}
		else if(e.getActionCommand().equalsIgnoreCase("s�, es el pais"))
		{
			this.view.crearPanelGana();
		}
		else if(e.getActionCommand().equalsIgnoreCase("no, no es el pais"))
		{
			this.view.crearPanelPierde(this.juego.getRespuesta());
		}
		else if(e.getActionCommand().equalsIgnoreCase("bien hecho"))
		{
			this.view.crearPanelJuegoTerminado();
		}
		else if(e.getActionCommand().equalsIgnoreCase("recordar"))
		{
			this.juego.recordarPreguntaYRespuesta(this.view.getNuevoPais(), this.view.getNuevaPregunta());
			this.juego.guardarDatos();
			this.view.crearPanelJuegoTerminado();
		}
		else if(e.getActionCommand().equalsIgnoreCase("salir"))
		{
			this.juego.guardarDatos();
			this.view.dispose();
		}
	}
	
	
//**************************************************************METODOS AUXILIARES
	
	
	private void actualizarPreguntaORespuesta() 
	{
		String pregunta = this.juego.getPregunta();
		
		if(pregunta != null)
		{
			this.view.mostrarPregunta(pregunta);
		}
		else
		{
			String resultado = this.juego.getRespuesta();
			this.view.crearPanelResultado(resultado);
		}
	}
	
	
	
	
	
	
	
	
	
	
}
