package model;

import static org.junit.Assert.assertNull;

//import static org.junit.Assert.*;

import org.junit.Test;

public class CaminoBinarioTest 
{
	
	
	@Test(expected = RuntimeException.class)
	public void indiceExedidoTest() 
	{
		CaminoBinario cb = new CaminoBinario();
		
		cb.siguienteDecision();
	}

	
	@Test
	public void caminoSinDecisiones() 
	{
		CaminoBinario cb = new CaminoBinario();
		assertNull(cb.getDecision());
	}
}
