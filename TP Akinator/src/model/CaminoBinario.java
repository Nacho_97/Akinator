package model;

import java.util.ArrayList;

public class CaminoBinario
{
	private ArrayList<Boolean> decisiones;
	private int indice;
	
	CaminoBinario()
	{
		this.decisiones = new ArrayList<Boolean>();
		this.indice = 0;
	}
	
	
	public void decidirIzquierda()
	{
		this.decisiones.add(true);
	}
	
	
	public void decidirDerecha()
	{
		this.decisiones.add(false);
	}
	
	
	public Boolean getDecision()
	{
		if(decisiones.size() != 0)
		{
			return this.decisiones.get(indice);
		}
		return null;
	}
	
	
	public String toString()
	{
		String ret = "Indice: " + this.indice + " Decisiones [";
		
		for(boolean bool : this.decisiones)
		{
			ret += bool;
		}
		return ret + "]";
	}
	
	
	public void siguienteDecision()
	{
		if(this.indice+1 > this.decisiones.size())
		{
			throw new RuntimeException("Se alcanzo el indice maximo");
		}
		else
		{
			this.indice = this.indice+1;
		}
	}
	
	
	public boolean yaSeReccorrioElCamino()
	{
		if(indice == this.decisiones.size())
		{
			return true;
		}
		return false;
	}


	public void reiniciarIndice() 
	{
		this.indice = 0;
	}
	
	
}
