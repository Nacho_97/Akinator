package model;

import datos.Datos;

public class Juego 
{
	private ArbolBinario cuestionario;
	private CaminoBinario decisiones;
	private Datos datos;
	
	public Juego()
	{
		this.datos = new Datos();
		this.cuestionario = datos.getArbol();
		this.decisiones = new CaminoBinario();
	}
	
	
	public void setCuestionario(ArbolBinario ab)
	{
		this.cuestionario = ab;
	}
	
	
	public String getPregunta()
	{
		String elemento = cuestionario.getElementoPorDecision(decisiones);
		
		if(this.chequearPreguntaORespuesta(elemento) == true)
		{
			return elemento;
		}
		else
		{
			return null;
		}
	}
	
	
	public String getRespuesta()
	{
		String elemento = cuestionario.getElementoPorDecision(decisiones);
		if(this.chequearPreguntaORespuesta(elemento) == false)
		{
			return elemento;
		}
		return null;
	}
	
	
	public void respuestaAfirmativa()
	{
		this.decisiones.decidirIzquierda();
	}
	
	
	public void respuestaNegativa()
	{
		this.decisiones.decidirDerecha();
	}
	
	
	public void reiniciarJuego() 
	{
		this.cuestionario = this.datos.getArbol();
		this.decisiones = new CaminoBinario();
	}
	
	
	public void recordarPreguntaYRespuesta(String pais, String pregunta)
	{
		String resultado = this.getRespuesta();
		pais = pais.toUpperCase();
		
		this.cuestionario.reemplazarElemento(pregunta, resultado);
		this.cuestionario.setDosElementosAPadre(pais, resultado, pregunta);
	}
	
	
	public void guardarDatos() 
	{
		this.datos.guardarDatos(cuestionario);
	}
	
	
//************************************************************************METODOS AUXILIARES
	
	
	boolean chequearPreguntaORespuesta(String str)
	{
		boolean ret = true;
		if(str == null)
		{
			throw new RuntimeException("No existe una respuesta");
		}
		else
		{
			String strMayus = str.toUpperCase();
			
			if(str.equals(strMayus))
			{
				ret = false;
			}
			
			return ret;
		}
	}


	/*private void corregirPreguntaORespuesta()	//ESTE METODO SIRVE PARA CORREGIR ALGUNA PREGUNTA O RESPUESTA MAL HECHA O ESCRITA
	{
		String preguntaACorregir = "�Su pais posee zalida al mar?";
		String sustituto = "�Su pais posee salida al mar?";
		
		this.cuestionario.reemplazarElemento(sustituto, preguntaACorregir);
	}*/


	
	
	
}
