package model;

import java.io.Serializable;

public class Nodo implements Serializable
{
	
	private static final long serialVersionUID = 1L;
	
	private String elemento;
	private Nodo izquierdo;
	private Nodo derecho;
	
	Nodo()
	{
		this.elemento = "";
		this.izquierdo = null;
		this.derecho = null;
	}
	
	Nodo(String elemento)
	{
		chequearElemento(elemento);
		this.elemento = elemento;
		this.izquierdo = null;
		this.derecho = null;
	}
	
	
	
	public String getElemento(){return this.elemento;}
	
	
	public Nodo getIzq(){return this.izquierdo;}
	
	
	public Nodo getDer(){return this.derecho;}
	
	
	
	public void setElemento(String informacion)
	{
		if(informacion != null)
		{
			this.elemento = informacion;
		}
		else
		{
			throw new IllegalArgumentException("Se intent๓ agregar un String en null");
		}
	}
	
	
	public void setIzq(Nodo n){this.izquierdo = n;}
	
	
	public void setDer(Nodo n){this.derecho = n;}
	
//***************************************************METODOS AUXILIARES
	
	public void chequearElemento(String str)
	{
		if(str == null)
		{
			throw new IllegalArgumentException("Si intent๓ inicializar un nodo con elemento en null");
		}
	}
	
	
}
