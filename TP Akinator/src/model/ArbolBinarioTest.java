package model;

import static org.junit.Assert.*;

import org.junit.Test;

public class ArbolBinarioTest {

	@Test
	public void elementoNoContenidoTest() 
	{
		ArbolBinario ab = new ArbolBinario();
		
		ab.setRaiz("pato");
		ab.setDosElementosAPadre("gato", "pescado", "pato");
		
		assertFalse(ab.estaContenido("perro"));
	}

	
	@Test
	public void elementoNoContenidoEnNullTest()
	{
		ArbolBinario ab = new ArbolBinario();
		
		assertFalse(ab.estaContenido(null));
	}
	
	
	@Test
	public void setDobleElementoTest()
	{
		ArbolBinario ab = new ArbolBinario();

		ab.setRaiz("raiz");
		ab.setDosElementosAPadre("pajaro", "tigre", "raiz");
		
		ab.setDosElementosAPadre("perro", "gato", "pajaro");
		ab.setDosElementosAPadre("tiburon", "pulpo", "tigre");
		
		assertTrue(ab.estaContenido("perro"));
		assertTrue(ab.estaContenido("gato"));
		assertTrue(ab.estaContenido("tiburon"));
		assertTrue(ab.estaContenido("pulpo"));
	}
	
	
	@Test
	public void setDobleElementoSinPadreExistenteTest()
	{
		ArbolBinario ab = new ArbolBinario();

		ab.setRaiz("raiz");
		ab.setDosElementosAPadre("pajaro", "tigre", "pulpo");
		
		assertFalse(ab.estaContenido("pulpo"));
		assertFalse(ab.estaContenido("pajaro"));
		assertFalse(ab.estaContenido("tigre"));
	}
	
	
	@Test(expected = IllegalArgumentException.class)
	public void setDobleElementoEnNullTest()
	{
		ArbolBinario ab = new ArbolBinario();

		ab.setRaiz("raiz");
		ab.setDosElementosAPadre(null, null, "raiz");
	}
	
	
	@Test(expected = IllegalArgumentException.class)
	public void setRaizNullTest()
	{
		ArbolBinario ab = new ArbolBinario();

		ab.setRaiz(null);
	}
	
	@Test
	public void remplazarElementoTest()
	{
		ArbolBinario ab = new ArbolBinario();

		ab.setRaiz("raiz");
		ab.reemplazarElemento("pato", "raiz");
		
		assertTrue(ab.estaContenido("pato"));
		assertFalse(ab.estaContenido("raiz"));
	}
	
	
	@Test(expected = IllegalArgumentException.class)
	public void remplazarElementoNullTest()
	{
		ArbolBinario ab = new ArbolBinario();

		ab.setRaiz("raiz");
		
		ab.reemplazarElemento(null, "raiz");
	}
	
	
	@Test
	public void remplazarElementoInexistenteTest()
	{
		ArbolBinario ab = new ArbolBinario();

		ab.setRaiz("raiz");
		ab.setDosElementosAPadre("pato", "mono", "raiz");
		ab.reemplazarElemento("jirafa", "leon");
		
		assertFalse(ab.estaContenido("leon"));
		assertFalse(ab.estaContenido("jirafa"));
	}
	
	
	@Test
	public void getElementoPorDecisionTest()
	{
		ArbolBinario ab = new ArbolBinario();
		CaminoBinario cb = new CaminoBinario();
		
		ab.setRaiz("mono");
		ab.setDosElementosAPadre("caballo", "gato", "mono");
		ab.setDosElementosAPadre("pulpo", "pez", "caballo");
		cb.decidirIzquierda();
		
		assertEquals("caballo",ab.getElementoPorDecision(cb));
	}
	
	
	@Test
	public void getElementoPorDecisionTestDoble()
	{
		ArbolBinario ab = new ArbolBinario();
		CaminoBinario cb = new CaminoBinario();
		
		ab.setRaiz("mono");
		ab.setDosElementosAPadre("caballo", "gato", "mono");
		ab.setDosElementosAPadre("pulpo", "pez", "caballo");
		cb.decidirIzquierda();
		
		assertEquals("caballo",ab.getElementoPorDecision(cb));
		assertEquals("caballo",ab.getElementoPorDecision(cb));
	}
	
	
	@Test 
	public void caminoMayorAlArbolTest()
	{
		ArbolBinario ab = new ArbolBinario();
		CaminoBinario cb = new CaminoBinario();
		
		ab.setRaiz("mono");
		ab.setDosElementosAPadre("caballo", "gato", "mono");
		cb.decidirIzquierda();
		cb.decidirDerecha();
		cb.decidirIzquierda();
		cb.decidirDerecha();
		cb.decidirIzquierda();
		
		assertNull(ab.getElementoPorDecision(cb));
	}
	
	
	@Test
	public void sinCaminoBinarioTest()
	{
		ArbolBinario ab = new ArbolBinario();
		CaminoBinario cb = new CaminoBinario();
		
		ab.setRaiz("mono");
		ab.setDosElementosAPadre("caballo", "gato", "mono");
		
		assertEquals("mono", ab.getElementoPorDecision(cb));
	}
	
	
	
	
	
	
	
	
	
	
	
}
