package model;

import java.io.Serializable;

public class ArbolBinario implements Serializable
{
	private static final long serialVersionUID = 1L;
	private Nodo raiz;
	
	
	public ArbolBinario()
	{
		this.raiz = null;
	}
	
	public ArbolBinario(Nodo raiz)
	{
		this.raiz = raiz;
	}
	
	
	
	public Nodo getRaiz()
	{
		return this.raiz;
	}
	
	
	public void setRaiz(String elem)
	{
		this.raiz = new Nodo(elem);
	}
	
	
	public boolean estaContenido(String str)
	{
		return contenido(str, this.raiz);
	}
	
	
	public String toString()
	{
		return this.imprimir(raiz);
	}
	
	
	public String getElementoPorDecision(CaminoBinario decisiones)
	{
		String ret = this.getElemento(this.raiz, decisiones);
		decisiones.reiniciarIndice();
		return ret;
	}
	
	
	public void reemplazarElemento(String sustituto, String aRemplazar)
	{
		this.raiz = this.reemplazar(sustituto, aRemplazar, this.raiz);
	}
	
	
	public void setDosElementosAPadre(String elem1, String elem2, String elementoPadre)
	{
		this.agregarDosElementos(elem1, elem2, elementoPadre, raiz);
		
	}
	

//*****************************************************************METODOS AUXILIARES
	

	private Nodo reemplazar(String sustituto, String aRemplazar, Nodo padre) 
	{
		if(padre != null)
		{
			if(padre.getElemento().equalsIgnoreCase(aRemplazar))
			{
				padre.setElemento(sustituto);
			}
			else
			{
				padre.setIzq(reemplazar(sustituto, aRemplazar, padre.getIzq()));
				padre.setDer(reemplazar(sustituto, aRemplazar, padre.getDer()));
			}
			return padre;
		}
		
		return null;
	}
	
	
	private Nodo agregarDosElementos(String elem1, String elem2, String elementoPadre, Nodo padre) 
	{
		if(padre != null)
		{
			if(padre.getElemento().equalsIgnoreCase(elementoPadre))
			{
				padre.setIzq(new Nodo(elem1));
				padre.setDer(new Nodo(elem2));
			}
			else
			{
				padre.setIzq(agregarDosElementos(elem1,elem2,elementoPadre,padre.getIzq()));
				padre.setDer(agregarDosElementos(elem1,elem2,elementoPadre,padre.getDer()));
			}
			return padre;
		}
		
		return null;
	}
	
	
	private String getElemento(Nodo n, CaminoBinario decisiones)
	{
		if(n != null)
		{
			if(decisiones.yaSeReccorrioElCamino())
			{
				return n.getElemento();
			}
			else
			{
				if(decisiones.getDecision() == true)
				{
					decisiones.siguienteDecision();
					return getElemento(n.getIzq(), decisiones);
				}
				else
				{
					decisiones.siguienteDecision();
					return getElemento(n.getDer(), decisiones);
				}
			}
		}
		return null;
	}
	
	
	private String imprimir(Nodo n)
	{
		if(n == null)
		{
			return "";
		}
		else
		{
			return imprimir(n.getIzq())+ n.getElemento() + imprimir(n.getDer());
		}
	}
	
	
	private boolean contenido(String str, Nodo n)
	{
		if(n != null)
		{
			if(n.getElemento().equalsIgnoreCase(str))
			{
				return true;
			}
			else
			{
				return contenido(str, n.getIzq()) || contenido(str, n.getDer());
			}
		}
		return false;
	}














}

