package model;

import static org.junit.Assert.*;

import org.junit.Test;

public class JuegoTest {

	@Test
	public void chequearPreguntaTest() 
	{
		Juego juego = new Juego();
		String pregunta = "�como estas?";
		
		assertTrue(juego.chequearPreguntaORespuesta(pregunta));
	}
	
	
	@Test
	public void chequearRespuestaTest() 
	{
		Juego juego = new Juego();
		String pregunta = "ARGENTINA";
		
		assertFalse(juego.chequearPreguntaORespuesta(pregunta));
	}
}
